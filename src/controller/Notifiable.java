package controller;

public interface Notifiable {
    void alert();
    void fetch();
}
