package controller;

public interface Extinguishable {
    void extinguish();
}
