package controller;

import app.AppContext;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import model.product.Order;

import java.net.URL;
import java.util.*;

public class HistoryController implements Initializable {

    @FXML
    TableView<Pair<String, Order>> tvHistory;

    @FXML TableColumn colSiret,
            colOrderId,
            colPrice,
            colDate;

    @FXML Button btnCancel,
            btnRefund;

    private ObservableList<Pair<String, Order>> observableOrders;


    public HistoryController() {}


    /**
     * action de retour (quitte la popup)
     */
    @FXML
    protected void back()
    {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }


    /**
     * action pour refund un order, le supprime de la liste
     * et rembourse le shop en tenant compte des frais bancaire
     */
    @FXML
    protected void refund()
    {
        Pair<String, Order> item = tvHistory.getSelectionModel().getSelectedItem();
        if(item != null)
        {
            String siret = item.getKey();
            Order order = item.getValue();
            HashMap<String, List<Order>> shopOrders = AppContext.getWholesaler().getShopOrders();
            // remboursement
            AppContext.getWholesaler().getPartner(siret).fund(order.getTotal());
            // suppression de l'order dans l'appcontect
            List<Order> orders = shopOrders.get(siret);
            orders.remove(order);
            observableOrders.remove(item);
        }

    }


    /**
     * initialisation de la tableview des orders
     */
    private void initTableViewOrders()
    {
        HashMap<String, List<Order>> shopOrders = AppContext.getWholesaler().getShopOrders();
        List<Pair<String, Order>> listOrders = new ArrayList<>();

        for(Map.Entry<String, List<Order>> entry : shopOrders.entrySet())
        {
            List<Order> orders = entry.getValue();
            for(Order order : orders)
            {
                if(order.getProcessed())
                {
                    listOrders.add(new Pair<>(entry.getKey(), order));
                }
            }
        }

        observableOrders = FXCollections.observableList(listOrders);
        tvHistory.setItems(observableOrders);

        tvHistory.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        // siret
        colSiret.setCellValueFactory(new PropertyValueFactory<>("key"));
        // product name / product category
        colOrderId.setCellValueFactory(new PropertyValueFactory<>("value"));
        colOrderId.setCellFactory(new Callback<TableColumn<Pair<String, Order>, Order>, TableCell<Pair<String, Order>, Order>>() {
            @Override
            public TableCell<Pair<String, Order>, Order> call(TableColumn<Pair<String, Order>, Order> tableColumn) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(Order order, boolean bool) {
                        super.updateItem(order, bool);
                        if (order != null) {
                            setText(""+order.getId());
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });

        // price
        colPrice.setCellValueFactory(new PropertyValueFactory<>("value"));
        colPrice.setCellFactory(new Callback<TableColumn<Pair<String, Order>, Order>, TableCell<Pair<String, Order>, Order>>() {
            @Override
            public TableCell<Pair<String, Order>, Order> call(TableColumn<Pair<String, Order>, Order> tableColumn) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(Order order, boolean bool) {
                        super.updateItem(order, bool);
                        if (order != null) {
                            setText(order.getTotal() + "€");
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });

        // order date
        colDate.setCellValueFactory(new PropertyValueFactory<>("value"));
        colDate.setCellFactory(new Callback<TableColumn<Pair<String, Order>, Order>, TableCell<Pair<String, Order>, Order>>() {
            @Override
            public TableCell<Pair<String, Order>, Order> call(TableColumn<Pair<String, Order>, Order> tableColumn) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(Order order, boolean bool) {
                        super.updateItem(order, bool);
                        if (order != null) {
                            setText(order.getDate().toString());
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initTableViewOrders();
    }
}
