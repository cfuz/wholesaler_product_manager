package controller;

import app.AppContext;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import log.Logger;
import model.bank.BankAccount;
import model.bank.network.BankingNetwork;
import model.business.shop.Shop;
import model.product.CatalogProduct;
import model.product.ConcreteProduct;
import model.product.Order;

import java.net.URL;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;


public class InfoController implements Initializable, Notifiable {

    @FXML Label lblName,
            lblSiret,
            lblHQSiret,
            lblNetwork,
            lblCountry,
            lblBalance;

    @FXML Button btnBack,
            btnProcess,
            btnAdd;

    @FXML TextField tfAmount;

    @FXML TableView<Map.Entry<ConcreteProduct, Integer>> tvStock;

    @FXML TableView<Map.Entry<CatalogProduct, Integer>> tvMore;

    @FXML TableView<Order> tvOrders;

    @FXML TableColumn<Map.Entry<ConcreteProduct, Integer>, String> colNameStock,
            colCatStock,
            colExpDate,
            colQtyStock;

    @FXML TableColumn<Order, String> colOrderID,
            colPrice,
            colDate;

    @FXML TableColumn<Map.Entry<CatalogProduct, Integer>, String> colName,
            colCat,
            colQty;

    private Shop shop;

    private ObservableList<Order> orders;

    private ObservableList<Map.Entry<ConcreteProduct, Integer>> stock;

    Order current;



    public InfoController() {
        AppContext.addNotifiable(this, AppContext.getNOTIFIABLE_INFO_INDEX());
        current = null;
    }


    /**
     * Ajoute des fonds directement sur le compte du Shop que l'on est en train de traiter
     */
    @FXML
    public void addFunds() {
        Logger.console("InfoController::addFunds()");
        if (tfAmount.getText().length() > 0) {
            shop.fund(Float.parseFloat(tfAmount.getText()));
            lblBalance.setText(Float.toString(shop.getBankAccount().getBalance()));
            tfAmount.setText("");
            Objects.requireNonNull(AppContext.getPartnersObservable()).setAll(AppContext.getWholesaler().getPartners());
        } else {
            Logger.console("There is no amount to add..");
        }
    }


    /**
     * Ferme la fenetre courante
     */
    @FXML
    public void back() {
        Logger.console("InfoController::back()");
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.close();
    }


    /**
     * Recupere l'entree selectionnee dans la table tvOrders et la fait traiter par le grossiste par l'intermediaire de
     * l'AppContext
     */
    @FXML
    protected void processShopOrders() {
        Logger.console("InfoController::processShopOrders()");
        Order selection = tvOrders.getSelectionModel().getSelectedItem();
        if (selection != null) {
            AppContext.getWholesaler().process(selection.getId());
            if (selection.getProcessed()) {
                orders.remove(selection);
                tvMore.getItems().clear();
                lblBalance.setText(Float.toString(shop.getBankAccount().getBalance()));
                stock.clear();
                stock.addAll(shop.getStock().entrySet());
            }
        } else {
            Logger.error("No order selected..");
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Logger.console("InfoController::initialize()");
        try {
            shop = AppContext.getWholesaler().getPartner(Objects.requireNonNull(AppContext.getCurrentShop()));
            BankAccount bankAccount = Objects.requireNonNull(shop).getBankAccount();
            BankingNetwork bankingNetwork = bankAccount.getBankingNetwork();
            lblName.setText(shop.getName());
            lblSiret.setText(shop.getSiret());
            lblHQSiret.setText(shop.getHqSiret().equals("") ? "Indie Cie." : shop.getHqSiret());
            lblNetwork.setText(bankingNetwork.toString());
            lblCountry.setText(bankingNetwork.country() == null ? "None" : bankingNetwork.country());
            lblBalance.setText(Float.toString(bankAccount.getBalance()));
            initStockTableView();
            initOrdersTableView();
        } catch (NullPointerException npe) {
            Logger.error("Tried to retrieve a Shop without SIRET..");
        }
    }


    /**
     * Initialise les observables sur chacune des colonnes contenues dans la TableView tvStock
     * sources:
     * https://stackoverflow.com/questions/31916041/how-do-i-use-a-map-as-a-tableview-observablemap-object-parameter
     */
    private void initStockTableView() {
        Logger.console("InfoController::initStockTableView()");
        stock = FXCollections.observableArrayList(shop.getStock().entrySet());
        tvStock.setItems(stock);
        tvStock.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        // BlackMagic :)
        colNameStock.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getKey()
                        .getCatalogProduct()
                        .getProduct()
                        .getName()
        ));
        colCatStock.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getKey()
                        .getCatalogProduct()
                        .getProduct()
                        .getCategory()
        ));
        colExpDate.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getKey()
                        .getExpirationDate()
                        .toString()
        ));
        colQtyStock.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getValue()
                        .toString()
        ));
    }


    /**
     * Initialise les observables sur chacune des colonnes contenues dans la TableView tvOrders
     * sources:
     * https://docs.oracle.com/javafx/2/ui_controls/table-view.htm
     */
    private void initOrdersTableView() {
        Logger.console("InfoController::initOrdersTableView()");
        orders = FXCollections.observableArrayList(
                shop.getOrders()
                        .stream()
                        .filter(order -> !order.getProcessed())
                        .collect(Collectors.toList())
        );
        tvOrders.setItems(orders);
        tvOrders.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tvOrders.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                current = newSelection;
                initMoreTableView();
            }
        });
        colOrderID.setCellValueFactory(new PropertyValueFactory<>("id"));
        colPrice.setCellValueFactory(new PropertyValueFactory<>("total"));
        colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
    }


    /**
     * Initialise la table tvMore permettant de visualiser le detail de la commande selectionnee dans tvOrders
     */
    private void initMoreTableView() {
        Logger.console("InfoController::initStockTableView()");
        ObservableList<Map.Entry<CatalogProduct, Integer>> items = FXCollections.observableArrayList(
                current.getCart()
                        .entrySet()
        );
        tvMore.setItems(items);
        tvMore.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        // BlackMagic :)
        colName.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getKey()
                        .getProduct()
                        .getName()
        ));
        colCat.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getKey()
                        .getProduct()
                        .getCategory()));
        colQty.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getValue()
                        .toString()
        ));
    }


    @Override
    public void alert() { }


    /**
     * Met a jour l'affichage du compte en banque du client des au'un paiement differe prend effet
     * source:
     * https://stackoverflow.com/questions/52315575/cant-update-the-label-in-javafx
     * --> maj label dans le contexte de l'application
     */
    @Override @FXML
    public void fetch() {
        Platform.runLater(() -> lblBalance.setText(Float.toString(shop.getBankAccount().getBalance())));
    }
}
