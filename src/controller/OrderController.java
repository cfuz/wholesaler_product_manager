package controller;

import app.AppContext;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import model.business.shop.Shop;
import model.product.CatalogProduct;
import model.product.Order;

import java.net.URL;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;



public class OrderController implements Initializable {

    @FXML Button btnAdd,
            btnDel,
            btnCancel,
            btnOrder;

    @FXML TableView<CatalogProduct> tvCatalog;

    @FXML TableView<Map.Entry<CatalogProduct, Integer>> tvCart;

    @FXML TableColumn<CatalogProduct, String> colName,
            colCat,
            colMinQty,
            colBasePrice;

    @FXML TableColumn<Map.Entry<CatalogProduct, Integer>, String> colCartName,
            colCartCat,
            colQty,
            colPrice;

    @FXML Label lblTotal,
            lblTotalCart;

    @FXML TextField tfQty;

    private Shop shop;

    private CatalogProduct selectedCatalogProduct;

    private int quantity;

    private HashMap<CatalogProduct, Integer> cart;

    private ObservableList<Map.Entry<CatalogProduct, Integer>> observableBasket;



    public OrderController() {}


    /**
     * action de quitter la popup
     */
    @FXML
    protected void back() {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }


    /**
     * mis à jour du prix total du produit pour la quantité demandé en fonction du shop
     * et de ses avantages
     */
    @FXML
    private void updateTotalPriceProduct() {
        selectedCatalogProduct = tvCatalog.getSelectionModel().getSelectedItem();
        if(selectedCatalogProduct != null)
        {
            float totalPrice = quantity * shop.discount(selectedCatalogProduct);
            lblTotal.setText("" + totalPrice);
        }
    }


    /**
     * ajout de l'article du panier
     */
    @FXML
    protected void addToCart() {
        if (selectedCatalogProduct != null) {
            cart.put(
                    selectedCatalogProduct,
                    cart.get(selectedCatalogProduct) != null ?
                            cart.get(selectedCatalogProduct) + quantity :
                            quantity
            );
            observableBasket.clear();
            observableBasket.addAll(cart.entrySet());
            float total = Float.parseFloat(lblTotalCart.getText());
            total += shop.discount(selectedCatalogProduct) * quantity;
            lblTotalCart.setText(Float.toString(total));
        }
    }


    /**
     * suppression de l'article du panier
     */
    @FXML
    protected void delFromCartSelection() {
        Map.Entry<CatalogProduct, Integer> item;
        if ((item = tvCart.getSelectionModel().getSelectedItem()) != null) {
            observableBasket.remove(item);
            cart.remove(item.getKey());
            float total = Float.parseFloat(lblTotalCart.getText());
            total -= shop.discount(item.getKey()) * item.getValue();
            lblTotalCart.setText(String.valueOf(total));
        }
    }


    /**
     * si il y a un panier, crée un order et l'envoie au wholesaler (la commande seulement, l'achat n'est pas confirmé)
     */
    @FXML
    protected void sendOrder() {
        if (observableBasket.size() > 0) {
            HashMap<CatalogProduct, Integer> basket = new HashMap<>();
            float totalPrice = 0.0f;
            for(Map.Entry<CatalogProduct, Integer> item : observableBasket) {
                basket.put(item.getKey(), item.getValue());
                totalPrice += shop.discount(item.getKey()) * item.getValue();
            }
            Order order = new Order(LocalDate.now(), basket, totalPrice, false);
            shop.order(order);
            back();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tfQty.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                quantity =  Integer.parseInt(newValue);
                if(selectedCatalogProduct != null)
                {
                    int minQuantity = shop.minimumQuantity(selectedCatalogProduct);
                    if(quantity < minQuantity) quantity = minQuantity;
                    updateTotalPriceProduct();
                }
            } catch (NumberFormatException e)
            {
                if(tfQty.getText().isEmpty())
                {

                }
                else if(selectedCatalogProduct != null)
                {
                    tfQty.setText("" + shop.minimumQuantity(selectedCatalogProduct));
                }
                else
                {
                    tfQty.setText("0");
                }
            }
        });
        initCatalogTableView();
        initBasketTableView();
    }


    /**
     * initialise la tableview du panier
     */
    private void initBasketTableView() {
        cart = new HashMap<>();
        observableBasket = FXCollections.observableArrayList(cart.entrySet());
        tvCart.setItems(observableBasket);
        tvCart.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        colCartName.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getKey()
                        .getProduct()
                        .getName()
        ));
        colCartCat.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getKey()
                        .getProduct()
                        .getCategory()
        ));
        colQty.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue()
                        .getValue()
                        .toString()
        ));
        colPrice.setCellValueFactory(item -> new SimpleStringProperty(
                Float.toString(
                        item.getValue().getValue() * shop.discount(item.getValue().getKey())
                )
        ));
    }


    /**
     * initialise la tableview des produits du catalogue du wholesaler en tenant compte des avantages ou non du shop concerné
     */
    private void initCatalogTableView() {
        shop = AppContext.getWholesaler().getPartner(AppContext.getCurrentShop());
        tvCatalog.setItems(AppContext.getCatalogProductsObservable());
        tvCatalog.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        colName.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue().getProduct().getName()
        ));
        colCat.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue().getProduct().getCategory()
        ));
        colMinQty.setCellValueFactory(item -> new SimpleStringProperty(
                Integer.toString(shop.minimumQuantity(item.getValue().getCatalogProduct()))
        ));
        colBasePrice.setCellValueFactory(item -> new SimpleStringProperty(
                Float.toString(shop.discount(item.getValue()))
        ));

        // listener pour récupérer le ctalog product sélectionné quand on clique sur une ligne
        tvCatalog.setRowFactory(tv -> {
            TableRow<CatalogProduct> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY ) {
                    selectedCatalogProduct = row.getItem();
                    tfQty.setText("" + shop.minimumQuantity(selectedCatalogProduct));
                }
            });
            return row ;
        });
    }
}
