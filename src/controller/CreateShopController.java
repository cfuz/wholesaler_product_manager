package controller;

import app.AppContext;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import log.Logger;
import model.bank.ClientBankAccount;
import model.bank.network.MasterCard;
import model.bank.network.Visa;
import model.bank.network.rate.*;
import model.business.shop.Shop;
import model.factory.FranchisedShopFactory;
import model.factory.IndieShopFactory;
import model.factory.ShopFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class CreateShopController implements Initializable {

    @FXML private TextField tfSiret,
            tfHQSiret,
            tfName,
            tfNEmployees,
            tfBalance,
            tfDiscount,
            tfMinQty,
            tfDeferred;

    @FXML private ComboBox<String> cbNetwork,
            cbRate;

    @FXML private Button btnDone,
            btnCancel;

    private ShopFactory shopFactory;


    public CreateShopController() {}

    /**
     * selectionne la bonne factory pour créer le shop correspondant
     * @param hqSiret "None" si c'est un shop indépendant, autre si c'est un franchisé
     */
    private void selectFactory(String hqSiret) {
        Logger.console("CreateShopController::selectFactory(" + hqSiret + ")");
        shopFactory = hqSiret.equals("None") ? IndieShopFactory.INSTANCE : FranchisedShopFactory.INSTANCE;
    }


    private CountryRate getRate(int i) {
        Logger.console("CreateShopController::getRate(" + i + ")");
        switch (i) {
            default:
                return new OthersRate();
            case 1:
                return new PortugualRate();
            case 2:
                return new SpainRate();
            case 3:
                return new UKRate();
            case 4:
                return new USARate();
        }
    }

    /**
     *  traite les données pour créer un nouveau shop à partir du formulaire
     *  prends des valeurs par défaut si champs erronés
     */
    public void processData() {
        Logger.console("CreateShopController::processData()");
        String siret = tfSiret.getText();

        String hqSiret = tfHQSiret.getText();
        selectFactory(hqSiret);

        String name = tfName.getText();

        int nEmployees;
        try {
            nEmployees = Integer.parseInt(tfNEmployees.getText());
        } catch (Exception e) {
            nEmployees = 1;
        }

        float balance;
        try {
            balance = Float.parseFloat(tfBalance.getText());
        } catch (Exception e) {
            balance = 1000.0f;
        }

        int defferedDay;
        try {
            defferedDay = Integer.parseInt(tfDeferred.getText());
        } catch (Exception e) {
            defferedDay = 10;
        }

        int bankNetwork = cbNetwork.getSelectionModel().getSelectedIndex();
        int countryRate = cbRate.getSelectionModel().getSelectedIndex();
        ClientBankAccount bankAccount = (bankNetwork == 1) ?
                new ClientBankAccount(balance, new MasterCard(), defferedDay) :
                new ClientBankAccount(balance, new Visa(getRate(countryRate)), defferedDay);

        float discount;
        try {
            discount = Float.parseFloat(tfDiscount.getText());
        } catch (Exception e) {
            discount = 0.0f;
        }

        float minQuantity;
        try {
            minQuantity = Float.parseFloat(tfMinQty.getText());
        } catch (Exception e) {
            minQuantity = 5;
        }

        Shop shop = shopFactory.create(
                siret,
                hqSiret,
                name,
                nEmployees,
                bankAccount,
                AppContext.getClock(),
                discount,
                minQuantity
        );

        AppContext.getWholesaler().addPartner(shop);
        AppContext.getPartnersObservable().add(shop);
        Stage stage = (Stage) btnDone.getScene().getWindow();
        stage.close();
    }

    /**
     * action pour quitter la popup
     */
    @FXML
    public void cancel() {
        Logger.console("CreateShopController::cancel()");
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }


    /**
     * affiche ou pas la combobox du pays (si Visa sélectionné)
     */
    @FXML
    public void toggleComboRate() {
        Logger.console("CreateShopController::toggleComboRate()");
        if (cbRate.isVisible()) {
            cbRate.setVisible(false);
        } else {
            cbRate.setVisible(true);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Logger.console("CreateShopController::initialize(...)");
        cbNetwork.getSelectionModel().selectFirst();
        cbRate.getSelectionModel().selectFirst();
        tfDiscount.setVisible(false);
        tfMinQty.setVisible(false);
        tfHQSiret.textProperty().addListener((observable, oldValue, newValue) -> {
            String str = tfHQSiret.getText();
            if(str.equals("None"))
            {
                tfDiscount.setVisible(false);
                tfMinQty.setVisible(false);
            }
            else
            {
                tfDiscount.setVisible(true);
                tfMinQty.setVisible(true);
            }
        });
    }
}
