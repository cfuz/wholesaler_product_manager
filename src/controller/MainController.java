package controller;

import app.AppContext;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import log.Logger;
import model.bank.BankAccount;
import model.bank.network.Visa;
import model.business.Wholesaler;
import model.business.shop.Shop;
import model.product.CatalogProduct;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;


public class MainController implements Initializable, Notifiable, Extinguishable {
    private int NEW_CATALOG_PRODUCT = 0,
            HISTORY = 1,
            SHOP_ORDER = 2,
            NEW_PARTNER = 3,
            SHOP_INFO = 4;

    private Stage[] openedPopups;

    @FXML private Stage window;

    @FXML private Button btnShopInfo;

    @FXML private Label lblBankAccountWS;

    @FXML private TableView<Shop> tvPartners;

    @FXML private TableView<CatalogProduct> tvCatalog;

    @FXML private TextField tfNameWS,
            tfSiretWS;

    @FXML private TableColumn<Shop, String> colShopName,
            colShopSiret,
            colShopSiretHQ,
            colBankAccount;

    @FXML private TableColumn<CatalogProduct, String> colProductName,
            colProductCategory,
            colMinimumQuantity,
            colBasePrice;

    @FXML private Text txtClock;



    public MainController() {
        Logger.console("MainController()");
        AppContext.addNotifiable(this, AppContext.getNOTIFIABLE_MAIN_INDEX());
        openedPopups = new Stage[5];
    }


    /**
     * Retourne à l'etat precedent (s'il y a au moins deux etats sauvegardés)
     */
    @FXML
    protected void previous() {
        initWindow();
        Logger.console("MainController::previous()");
        int state = AppContext.getWholesaler().getMemento().getBackups().size();
        Logger.console("# saved states: " + state);
        if (state > 1) {
            extinguish();
            AppContext.getWholesaler().previousState();
            Objects.requireNonNull(AppContext.getPartnersObservable()).setAll(AppContext.getWholesaler().getPartners());
            Objects.requireNonNull(AppContext.getCatalogProductsObservable()).setAll(AppContext.getWholesaler().getCatalog());
            initLabels();
        }
    }

    /**
     * Affiche la fenetre permettant la creation de nouveaux produits dans le catalogue des ventes du grossiste
     * @throws IOException ...
     */
    @FXML
    protected void addCatalogProduct() throws IOException {
        initWindow();
        Logger.console("MainController::addCatalogProduct()");
        if (this.openedPopups[NEW_CATALOG_PRODUCT] == null) {
            this.openedPopups[NEW_CATALOG_PRODUCT] = fire(
                    "../view/order_ws.fxml",
                    "Order",
                    window.getX() + window.getWidth() + 10,
                    window.getY() + window.getHeight() + 10,
                    we -> this.openedPopups[NEW_CATALOG_PRODUCT] = null,
                    we -> this.openedPopups[NEW_CATALOG_PRODUCT] = null
            );
        } else {
            Logger.error("WS catalog's window is already shown..");
        }
    }


    /**
     * Affiche la fenetre d'historique des commandes realisees par le grossiste
     * @throws IOException ...
     */
    @FXML
    protected void history() throws IOException {
        initWindow();
        Logger.console("MainController::history()");
        if (this.openedPopups[HISTORY]  == null) {
            this.openedPopups[HISTORY] = fire(
                    "../view/hist.fxml",
                    "History",
                    window.getX() + window.getWidth() + 10,
                    window.getY(),
                    we -> this.openedPopups[HISTORY] = null,
                    we -> this.openedPopups[HISTORY] = null
            );
        } else {
            Logger.error("WS history's window is already shown..");
        }
    }


    /**
     * Affiche la fenetre de commande pour un partenaire selectionne dans la liste du menu principal.
     * @throws IOException ...
     */
    @FXML
    protected void orderShop() throws IOException {
        Logger.console("MainController::orderShop()");
        initWindow();
        if (this.openedPopups[SHOP_ORDER] == null) {
            Shop selectedShop = tvPartners.getSelectionModel().getSelectedItem();
            if (selectedShop != null) {
                AppContext.setCurrentShop(selectedShop.getSiret());
                Logger.console("Selected Shop's siret : " + selectedShop.getSiret());
                this.openedPopups[SHOP_ORDER] = fire(
                        "../view/order.fxml",
                        "Shop - Order - SIRET : " + selectedShop.getSiret(),
                        window.getX() - window.getWidth() - 10,
                        window.getY(),
                        we -> {
                            AppContext.setCurrentShop(null);
                            this.openedPopups[SHOP_ORDER] = null;
                        },
                        we -> {
                            AppContext.setCurrentShop(null);
                            this.openedPopups[SHOP_ORDER] = null;
                        }
                );
            }
            else
            {
                Logger.error("No selected shop !");
            }
        } else {
            Logger.error("Shop order's window is already shown..");
        }
    }


    /**
     * Affiche la fenetre de creation de nouvelle instance d'entreprise partenaire.
     * @throws IOException ...
     */
    @FXML
    protected void newPartner() throws IOException {
        Logger.console("MainController::newPartner()");
        initWindow();
        if (this.openedPopups[NEW_PARTNER] == null) {
            this.openedPopups[NEW_PARTNER] = fire(
                    "../view/createshop.fxml",
                    "Shop - Create",
                    window.getX() - window.getWidth() - 10,
                    window.getY() + window.getHeight() + 10,
                    we -> this.openedPopups[NEW_PARTNER] = null,
                    we -> this.openedPopups[NEW_PARTNER] = null
            );
        } else {
            Logger.error("New partner's window is already shown..");
        }
    }


    /**
     * Supprime un partenaire selectionne dans la table tvPartners
     */
    @FXML
    protected void deletePartner() {
        initWindow();
        Logger.console("MainController::deletePartner()");
        Shop selectedShop = tvPartners.getSelectionModel().getSelectedItem();
        if (selectedShop != null) {
            Logger.console("Selected Shop's siret : " + selectedShop.getSiret());
            AppContext.getWholesaler().removePartner(selectedShop);
            Objects.requireNonNull(AppContext.getPartnersObservable()).remove(selectedShop);
        }
    }


    /**
     * traite toutes les commancdes
     */
    @FXML
    protected void processAll()
    {
        AppContext.getWholesaler().processAll();
        extinguish();
    }

    /**
     * Supprime un produit du catalogue du grossiste
     */
    @FXML
    protected void deleteCatalogProduct() {
        initWindow();
        Logger.console("MainController::deleteCatalogProduct()");
        CatalogProduct selectedCatalogProduct = tvCatalog.getSelectionModel().getSelectedItem();
        if (selectedCatalogProduct != null) {
            AppContext.getWholesaler().removeCatalogProduct(selectedCatalogProduct);
            Objects.requireNonNull(AppContext.getCatalogProductsObservable()).remove(selectedCatalogProduct);
        }
    }


    /**
     * Notifie des que l'horloge du contexte est mise a jour.
     */
    @Override
    public void alert() {
        txtClock.setText(AppContext.getClock().toString());
    }


    /**
     * Apres selection du partenaire, accede aux details de celui-ci par l'intermediaire d'une nouvelle fenetre.
     * Cette fenetre permet entre autres, d'ajouter des fonds au commerce, et de traiter les commandes qu'il a en
     * attente.
     * @throws IOException  ...
     */
    @FXML
    protected void showShopInfo() throws IOException {
        initWindow();
        Logger.console("MainController::showShopInfo()");
        if (openedPopups[SHOP_INFO] == null) {
            Shop selection = tvPartners.getSelectionModel().getSelectedItem();
            if (selection != null) {
                AppContext.setCurrentShop(selection.getSiret());
                this.openedPopups[SHOP_INFO] = fire(
                        "../view/info.fxml",
                         "Shop - Information",
                        window.getX(),
                        window.getY() + window.getHeight() + 10,
                        we -> {
                            AppContext.setCurrentShop(null);
                            this.openedPopups[SHOP_INFO] = null;
                            AppContext.removeNotifiable(AppContext.getNOTIFIABLE_INFO_INDEX());
                        },
                        we -> {
                            AppContext.setCurrentShop(null);
                            this.openedPopups[SHOP_INFO] = null;
                            AppContext.removeNotifiable(AppContext.getNOTIFIABLE_INFO_INDEX());
                        }
                );
            } else {
                Logger.error("No shop selected..");
            }
        } else {
            Logger.error("Partner's information window is already shown..");
        }
    }


    /**
     * Lance une nouvelle instance de fenetre a partir d'un fichier fxml renseigne dans les parametres
     * @param pathToResource        chemin vers le fichier fxml
     * @param title                 titre de la fenetre
     * @param offset_x              position horizontale sur l'ecran
     * @param offset_y              position verticale sur l'ecran
     * @param onCloseEventHandler   action a realiser en cas de fermeture de la fenetre ouverte
     * @param onHiddenEventHandler  action a declencher en cas de fermeture via le bouton x de la fenetre
     * @return                      instance de la fenetre generee
     * @throws IOException          des que l'ouverture genere un erreur
     */
    private Stage fire(
            String pathToResource,
            String title,
            double offset_x,
            double offset_y,
            EventHandler<WindowEvent> onCloseEventHandler,
            EventHandler<WindowEvent> onHiddenEventHandler
    ) throws IOException {
        Logger.console("MainController::fireWindow()");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(pathToResource));
        Parent root = fxmlLoader.load();
        Stage popup = new Stage();
        popup.setTitle(title);
        popup.setScene(new Scene(root));
        popup.setX(offset_x);
        popup.setY(offset_y);
        popup.setOnCloseRequest(onCloseEventHandler);
        popup.setOnHidden(onHiddenEventHandler);
        popup.show();
        return popup;
    }


    /**
     * Definit la reference vers la fenetre principale de l'application si celle-ci n'est pas deja instanciee
     */
    private void initWindow() {
        if (window == null) {
            Logger.console("Init. window ref.");
            window = (Stage) btnShopInfo.getScene().getWindow();
        }
    }


    /**
     * Ferme toutes les fenetres ouvertes dans le contexte de l'application
     */
    @Override
    public void extinguish() {
        Logger.console("MainController::extinguish()");
        for (Stage stage : openedPopups) {
            if (stage != null) {
                stage.close();
            }
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Logger.console("MainController::initialize()");
        txtClock.setText(AppContext.getClock().toString());
        initLabels();
        initCatalogTableView();
        initPartnersTableView();
    }

    /**
     * initialise les infos bancaires du grossiste
     */
    private void initLabels() {
        Wholesaler wholesaler = AppContext.getWholesaler();
        // Information
        tfNameWS.setText(wholesaler.getName());
        tfSiretWS.setText(wholesaler.getSiret());
        BankAccount bankAccount = wholesaler.getBankAccount();
        String strTypeNetwork = bankAccount.getBankingNetwork().getClass().getSimpleName();
        // Bank account
        String infoAccount;
        if (strTypeNetwork.equals("Visa")) {
            Visa visa = (Visa) bankAccount.getBankingNetwork();
            infoAccount = "Balance : " + bankAccount.getBalance() + "\n" +
                    "Bank Network : " + visa.getClass().getSimpleName() + "\n" +
                    "Country : " + visa.getCountryRate().getClass().getSimpleName().replace("Rate", "");
        } else {
            infoAccount = "Balance : " + bankAccount.getBalance() + "\n" +
                    "Bank Network : " + bankAccount.getBankingNetwork().getClass().getSimpleName();
        }
        lblBankAccountWS.setText(infoAccount);
    }


    /**
     * initialise la tableview des produits du catalog du wholesaler (catalogproduct)
     */
    private void initCatalogTableView() {
        tvCatalog.setItems(AppContext.getCatalogProductsObservable());
        tvCatalog.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tvCatalog.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                tvPartners.getSelectionModel().clearSelection();
            }
        });
        colProductName.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue().getProduct().getName()
        ));
        colProductCategory.setCellValueFactory(item -> new SimpleStringProperty(
                item.getValue().getProduct().getCategory()
        ));
        colMinimumQuantity.setCellValueFactory(new PropertyValueFactory<>("minimumQuantity"));
        colBasePrice.setCellValueFactory(new PropertyValueFactory<>("basePrice"));
    }


    /**
     * initialise la tableview des partners du wholesaler (shops)
     */
    private void initPartnersTableView() {
        tvPartners.setItems(AppContext.getPartnersObservable());
        tvPartners.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tvPartners.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                tvCatalog.getSelectionModel().clearSelection();
            }
        });
        colShopName.setCellValueFactory(new PropertyValueFactory<>("name"));
        colShopSiret.setCellValueFactory(new PropertyValueFactory<>("siret"));
        colShopSiretHQ.setCellValueFactory(new PropertyValueFactory<>("hqSiret"));
        colBankAccount.setCellValueFactory(item -> new SimpleStringProperty(
                Float.toString(item.getValue().getBankAccount().getBalance())
        ));
    }


    /**
     * Met a jour l'affichage des informations du grossiste des que de nouvelles informations sont notifiees
     */
    @Override public void fetch() {
        Platform.runLater(this::initLabels);
        Objects.requireNonNull(AppContext.getPartnersObservable()).setAll(AppContext.getWholesaler().getPartners());
    }
}
