package controller;

import app.AppContext;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import log.Logger;
import model.business.Wholesaler;
import model.product.CatalogProduct;

import java.net.URL;
import java.util.ResourceBundle;



public class OrderWSController implements Initializable {

    @FXML Button btnAdd,
            btnCancel;

    @FXML TextField tfName,
            tfCategory,
            tfDuration,
            tfBasePrice,
            tfMinQty;



    public OrderWSController() { }



    @Override
    public void initialize(URL location, ResourceBundle resources) { }

    /**
     * création d'un nouveau produit dans le catalogue
     */
    @FXML
    protected void add() {
        Logger.console("OrderWSController::add()");
        Wholesaler ws = AppContext.getWholesaler();
        CatalogProduct catalogProduct = ws.addToCatalog(
                tfName.getText(),
                tfCategory.getText(),
                Integer.parseInt(tfDuration.getText()),
                Float.parseFloat(tfBasePrice.getText()),
                Integer.parseInt(tfMinQty.getText())
        );
        AppContext.getCatalogProductsObservable().add(catalogProduct);
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }


    /**
     * action de quitter la popup
     */
    @FXML
    public void back() {
        Logger.console("OrderWSController::back()");
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }
}
