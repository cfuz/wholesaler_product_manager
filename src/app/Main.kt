import app.AppContext
import controller.Extinguishable
import javafx.application.Application
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import log.Logger



class Main : Application() {

    override fun start(primaryStage: Stage) {
        val fxmlLoader = FXMLLoader(javaClass.getResource("view/main.fxml"))
        val root: Parent = fxmlLoader.load()
        // On sait que le controller intégré dans la primaryStage implémente l'interface Extinguishable
        val controller: Extinguishable = fxmlLoader.getController()
        primaryStage.title = "Menu"
        primaryStage.scene = Scene(root)
        primaryStage.onCloseRequest = EventHandler {
            controller.extinguish()
        }
        primaryStage.isResizable = false
        primaryStage.show()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Logger
            AppContext
            launch(Main::class.java)
        }
    }
}