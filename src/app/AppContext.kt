@file:JvmName("AppContext")
package app

import controller.Notifiable
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import model.business.Wholesaler
import model.business.shop.Shop
import model.product.CatalogProduct
import java.time.LocalDate
import java.time.Period
import java.util.concurrent.TimeUnit


object AppContext {

    @JvmStatic
    val NOTIFIABLE_MAIN_INDEX = 0

    @JvmStatic
    val NOTIFIABLE_INFO_INDEX = 1

    @JvmStatic
    var clock: LocalDate = LocalDate.now()

    @JvmStatic
    val wholesaler = Wholesaler

    @JvmStatic
    var currentShop: String? = null

    @JvmStatic
    var exit = false

    @JvmStatic
    var notifiables = kotlin.arrayOfNulls<Notifiable>(2)

    @JvmStatic
    var partnersObservable: ObservableList<Shop>? = FXCollections.observableArrayList(wholesaler.partners)

    @JvmStatic
    var catalogProductsObservable: ObservableList<CatalogProduct>? = FXCollections.observableArrayList(wholesaler.catalog)



    private val thread: Thread = Thread(Runnable {
        while (!exit) {
            TimeUnit.SECONDS.sleep(1L)
            clock = clock.plus(Period.of(0,0,1))
            notifiables[NOTIFIABLE_MAIN_INDEX]?.alert()
            wholesaler.deferredDebit(clock)
        }
    })



    init {
        thread.isDaemon = true
        thread.start()
    }

    @JvmStatic
    fun addNotifiable(notifiable: Notifiable, index: Int) {
        notifiables[index] = notifiable
    }

    @JvmStatic
    fun removeNotifiable(index: Int) {
        notifiables[index] = null
    }

    @JvmStatic
    fun fetchAll() {
        for (notifiable in notifiables) {
            notifiable?.fetch()
        }
    }

}