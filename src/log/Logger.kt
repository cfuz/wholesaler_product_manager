package log

import app.AppContext


object Logger {
    @JvmStatic
    fun console(message: String) {
        println("➜ [${AppContext.clock}] $message")
    }

    @JvmStatic
    fun error(errorMessage: String) {
        println("➜ [${AppContext.clock}] ✘ $errorMessage")
    }
}
