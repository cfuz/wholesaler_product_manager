package model.product

import app.AppContext
import java.time.LocalDate

data class ConcreteProduct(
       val catalogProduct: CatalogProduct
) {
    val expirationDate: LocalDate = AppContext.clock.plusDays(this.catalogProduct.duration.toLong())
}