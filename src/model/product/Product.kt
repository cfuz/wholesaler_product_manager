package model.product


data class Product(
        val name: String,
        val category: String
)