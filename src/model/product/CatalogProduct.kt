package model.product


data class CatalogProduct(
        val product: Product,
        var duration: Int, // Nombre de jours avant péremption du produit
        var minimumQuantity: Int,
        var basePrice: Float
) {
    fun getCatalogProduct(): CatalogProduct {
        return this
    }
}