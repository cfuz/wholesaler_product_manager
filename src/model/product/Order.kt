package model.product

import java.time.LocalDate


data class Order(
        var date: LocalDate,
        var cart: HashMap<CatalogProduct, Int>,
        var total: Float,
        var processed: Boolean = false
) {
    val id: Int

    companion object {
        @JvmStatic
        protected var N_INSTANCES: Int = 0
    }


    init {
        id = N_INSTANCES
        N_INSTANCES++
    }
}