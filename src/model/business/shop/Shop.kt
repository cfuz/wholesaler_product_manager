package model.business.shop

import app.AppContext
import log.Logger
import model.bank.BankAccount
import model.business.Business
import model.business.Wholesaler
import model.product.CatalogProduct
import model.product.Order
import model.product.ConcreteProduct
import java.time.LocalDate



abstract class Shop(
        siret: String,
        name: String,
        nEmployees: Int,
        bankAccount: BankAccount,
        _firstOrder: LocalDate = AppContext.clock
): Business(siret, name, nEmployees, bankAccount) {

    var hqSiret = ""

    var firstOrder = _firstOrder

    val partner = Wholesaler

    var orders: MutableList<Order> = ArrayList();



    /**
     * Traite la commande recue en l'inserant dans les stocks
     * @param parcel    Contenu de la livraison
     */
    fun receive(parcel: MutableList<Pair<ConcreteProduct, Int>>) {
        Logger.console("Shop#$siret::receive(...)")
        for ((concreteProduct, quantity) in parcel) {
            stock[concreteProduct]
                    ?.plus(quantity)
                    ?:stock.put(concreteProduct, quantity)
        }
    }


    /**
     * Transmet une commande au grossiste
     * @param order contenu de la commande
     */
    fun order(order: Order) {
        Logger.console("Shop#$siret::order(...)")
        partner.order(siret, order)
        orders.add(order)
    }


    /**
     * Ajoute de l'argent au commerce
     * @param amount montant à ajouter au compte en banque
     */
    fun fund(amount: Float) {
        Logger.console("Shop#$siret::fund($amount)")
        bankAccount.balance += amount
    }


    abstract fun minimumQuantity(catalogProduct: CatalogProduct): Int


    abstract fun discount(catalogProduct: CatalogProduct): Float
}