package model.business.shop.fidelity

import app.AppContext
import model.business.shop.IndieShop


class TwoToFiveYears: Fidelity() {
    override val minimumQuantity: Boolean = true
    override val priceDiscount: Float = 0.1f


    override fun update(shop: IndieShop) {
        if (AppContext.clock.isAfter(shop.firstOrder.plusYears(5))) {
            shop.fidelity = TwoToFiveYears()
        }
    }
}