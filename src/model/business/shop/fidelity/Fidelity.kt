package model.business.shop.fidelity

import model.business.shop.IndieShop
import model.product.CatalogProduct


abstract class Fidelity {
    abstract val minimumQuantity: Boolean
    abstract val priceDiscount: Float


    fun minimumQuantity(shop: IndieShop, catalogProduct: CatalogProduct): Int {
        update(shop)
        return if (minimumQuantity) {
            catalogProduct.minimumQuantity
        } else {
            0
        }
    }

    fun discount(shop: IndieShop, catalogProduct: CatalogProduct): Float {
        update(shop)
        return catalogProduct.basePrice - (catalogProduct.basePrice * priceDiscount)
    }

    abstract fun update(shop: IndieShop)
}