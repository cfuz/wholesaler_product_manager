package model.business.shop.fidelity

import app.AppContext
import model.business.shop.IndieShop


class LessThanTwoYears: Fidelity() {
    override val minimumQuantity: Boolean = true
    override val priceDiscount: Float = 0.0f


    override fun update(shop: IndieShop) {
        if (AppContext.clock.isAfter(shop.firstOrder.plusYears(2))) {
            shop.fidelity = TwoToFiveYears()
        }
    }
}