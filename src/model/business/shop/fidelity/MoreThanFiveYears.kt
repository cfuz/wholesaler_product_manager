package model.business.shop.fidelity

import model.business.shop.IndieShop


class MoreThanFiveYears: Fidelity() {
    override val minimumQuantity: Boolean = false
    override val priceDiscount: Float = 0.2f


    override fun update(shop: IndieShop) {  }
}
