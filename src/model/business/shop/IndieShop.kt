package model.business.shop

import model.bank.BankAccount
import java.time.LocalDate
import model.business.shop.fidelity.Fidelity
import model.product.CatalogProduct


class IndieShop(
        siret: String,
        name: String,
        nEmployees: Int,
        bankAccount: BankAccount,
        firstOrder: LocalDate,
        _fidelity: Fidelity
) : Shop(siret, name, nEmployees, bankAccount, firstOrder) {
    var fidelity = _fidelity


    override fun minimumQuantity(catalogProduct: CatalogProduct): Int {
        return fidelity.minimumQuantity(this, catalogProduct)
    }

    override fun discount(catalogProduct: CatalogProduct): Float {
        return fidelity.discount(this, catalogProduct)
    }
}