package model.business.shop

import model.bank.BankAccount
import model.product.CatalogProduct
import java.time.LocalDate


class FranchisedShop(
        siret: String,
        _hqSiret: String,
        name: String,
        nEmployees: Int,
        bankAccount: BankAccount,
        firstOrder: LocalDate,
        _discount: Float = 0.0f,
        _minimumQuantity: Float = 0.0f
) : Shop(siret, name, nEmployees, bankAccount, firstOrder) {

    var discount = _discount

    var minimumQuantity = _minimumQuantity



    init {
        hqSiret = _hqSiret
    }

    override fun minimumQuantity(catalogProduct: CatalogProduct): Int {
        return (catalogProduct.minimumQuantity * (1.0f - minimumQuantity)).toInt()
    }


    override fun discount(catalogProduct: CatalogProduct): Float {
        val calc = catalogProduct.basePrice - discount
        if (calc < 0)
            return 1.0f
        return calc
    }
}