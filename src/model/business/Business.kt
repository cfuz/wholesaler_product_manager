package model.business

import model.bank.BankAccount
import model.product.ConcreteProduct


/**
 * L'annotation var devant un attribut permet de générer un getter et un setter implicites.
 * L'annotation val quant à elle génère uniquement une méthode d'accès implicite
 */
abstract class Business(_siret: String, _name: String, _nEmployees: Int, _bankAccount: BankAccount) {
    val siret: String = _siret
    val name: String = _name
    var nEmployees: Int = _nEmployees
    var bankAccount: BankAccount = _bankAccount
    var stock = HashMap<ConcreteProduct, Int>()


    fun updateStock(concreteProduct: ConcreteProduct, quantity: Int) {
        stock[concreteProduct]?.plus(quantity)
    }
}