package model.business

import app.AppContext
import log.Logger
import model.bank.ClassicBankAccount
import model.bank.ClientBankAccount
import model.bank.network.MasterCard
import model.bank.network.Visa
import model.bank.network.rate.SpainRate
import model.business.memento.Memento
import model.business.shop.Shop
import model.factory.FranchisedShopFactory
import model.factory.IndieShopFactory
import model.factory.ProductFactory
import model.product.CatalogProduct
import model.product.Order
import model.product.ConcreteProduct
import java.lang.Exception
import java.time.LocalDate



/**
 * Classe modélisant de le grossiste de l'application.
 * Un supposera qu'il ne peut y avoir qu'un seul grossiste pour l'application, c'est pourquoi on le référence par le mot
 * clef 'object' pour signifier que c'est un SINGLETON. Elle hérite d'autre part de la classe abstraite Business.
 * @property partners Liste de enseignes affiliées au grossiste
 * @property shopOrders Liste des commandes par enseigne (shop) référencées par leur numéro de SIRET : { siret -> [Orders] }
 * @property processed Flag permettant de savoir si oui ou non le grossiste à des commandes en attente de traitement
 */
object Wholesaler: Business(
        "00000000000001",
        "Berry Cie.",
        100,
        ClassicBankAccount(
                100000f,
                MasterCard()
        )
) {

    var partners: MutableList<Shop> = ArrayList()
    var shopOrders = HashMap<String, MutableList<Order>>()
    var processed = true
    var catalog: MutableList<CatalogProduct> = ArrayList()
    var memento = Memento()


    /**
     * Initialise le grossiste avec des produits dans son catalogue ainsi qu'un ensemble de clients (pratique pour les
     * tests)
     */
    init {
        partners.addAll(mutableListOf(
                FranchisedShopFactory.create(
                        "00000000000002",
                        "00000000000022",
                        "Some Franchised Shop",
                        2,
                        ClientBankAccount(
                                10000f,
                                Visa(SpainRate()),
                                10
                        ),
                        AppContext.clock,
                        0.1f,
                        0.25f
                ),
                IndieShopFactory.create(
                        "00000000000003",
                        "",
                        "Some Indie Shop",
                        1,
                        ClientBankAccount(
                                5000f,
                                MasterCard(),
                                14
                        ),
                        AppContext.clock
                )
        ))
        catalog.addAll(mutableListOf(
                CatalogProduct(
                        ProductFactory.get("strawberry", "cat. 3"),
                        7,
                        5,
                        10f
                ),
                CatalogProduct(
                        ProductFactory.get("blueberry", "cat. 2"),
                        7,
                        5,
                        8f
                ),
                CatalogProduct(
                        ProductFactory.get("cranberry", "cat. 2"),
                        7,
                        5,
                        9f
                ),
                CatalogProduct(
                        ProductFactory.get("chuckberry", "cat. 10"),
                        15,
                        10,
                        2.05f
                ),
                CatalogProduct(
                        ProductFactory.get("blackberry", "cat. 1"),
                        720,
                        1,
                        600f
                ),
                CatalogProduct(
                        ProductFactory.get("raspberry", "cat. 10"),
                        365,
                        1,
                        30f
                )
        ))
        memento.save(this)
    }


    fun previousState() {
        Logger.console("Wholesaler::previousState()")
        val state = memento.previousState()
        if (state != null) {
            Logger.console("#remaining states: ${memento.backups.size}")
            partners = state.partners
            shopOrders = state.shopOrders
            catalog = state.catalog
            bankAccount = state.bankAccount
        }
    }


    /**
     * Ajoute un produit au catalogue du grossiste
     * @param name nom du produit
     * @param category catégorie du produit
     * @param basePrice prix de base du produit
     * @param minimumQuantity quantité minimale d'achat
     * @param quantity quantité demandée
     */
    fun addToCatalog(
            name: String,
            category: String,
            duration: Int,
            basePrice: Float,
            minimumQuantity: Int
    ): CatalogProduct {
        Logger.console("Wholesaler::addToCatalog(...)")
        val product = ProductFactory.get(name, category)
        val catalogProduct = CatalogProduct(
                product,
                duration,
                minimumQuantity,
                basePrice
        )
        catalog.add(catalogProduct)
        memento.save(this)
        return catalogProduct
    }


    /**
     * Supprime le produit renseigne en parametre du catalogue du grossiste
     * @param catalogProduct    produit du catalogue a retirer
     */
    fun removeCatalogProduct(catalogProduct: CatalogProduct) {
        Logger.console("Wholesaler::removeCatalogProduct(...)")
        catalog.remove(catalogProduct)
        memento.save(this)
    }


    /**
     * Mise à jour du stock du grossiste par un appel à la fabrique de produits
     * @param name nom du produit
     * @param category catégorie du produit
     * @param basePrice prix de base du produit
     * @param minimumQuantity quantité minimale d'achat
     * @param expirationDate date d'expiration du produit
     * @param quantity quantité demandée
     */
    fun addProductToStock(
            name: String,
            category: String,
            duration: Int,
            basePrice: Float,
            minimumQuantity: Int,
            quantity: Int
    ) {
        Logger.console("Wholesaler::addProductToStock(...)")
        val product = ProductFactory.get(name, category)
        val catalogProduct = CatalogProduct(product, duration, minimumQuantity, basePrice)
        val concreteProduct = ConcreteProduct(catalogProduct)
        updateStock(concreteProduct, quantity)
    }


    /**
     * Ajoute la commande à la liste des commandes à traiter
     * @param siret n° d'identification de l'entreprise émettrice de la commande
     * @param order contenu de la commande
     */
    fun order(siret: String, order: Order) {
        Logger.console("Wholesaler::order(...)")
        shopOrders[siret]
                ?.add(order)
                ?:shopOrders.put(siret, mutableListOf(order))
        memento.save(this)
    }


    /**
     * Supprime un partenaires
     * @param partner partenaire du grossiste à supprimer
     */
    fun removePartner(partner: Shop) {
        Logger.console("Wholesaler::removePartner(...)")
        if (partners.isNotEmpty()) {
            partners.remove(partner)
            memento.save(this)
        }
    }


    /**
     * Ajoute un collaborateur à la liste du grossiste
     * @param shop Nouveau collaborateur
     */
    fun addPartner(shop: Shop) {
        Logger.console("Wholesaler::addPartner(...)")
        partners.add(shop)
        memento.save(this)
    }


    /**
     * Vérifie si un produit est disponible (présent en stock et en quantité suffisante) ou non.
     * Retourne le produit spécifié si disponible
     * @param catalogProduct produit à vérifier
     * @param quantity quantité à retirer
     * @return true si produit disponible, faux sinon
     */
    fun withdraw(catalogProduct: CatalogProduct, quantity: Int): Pair<ConcreteProduct, Int>? {
        Logger.console("Wholesaler::withdraw(...)")
        // On genere le produit concret depuis la fabrique de produit pour alimenter les stocks
        stock.put(ConcreteProduct(catalogProduct), quantity)
        // On parcours la liste des produits en stock
        for ((concreteProduct, _quantity) in stock) {
            if ((concreteProduct.catalogProduct == catalogProduct) and (quantity <= _quantity)) {
                // Si le produit est périmé on le retire du stock
                if (concreteProduct.expirationDate.isBefore(AppContext.clock)) {
                    stock.remove(concreteProduct)
                } else { // Sinon on renvoie un paire (produit_spécifique, qté)
                    // On met à jour le stock en soustrayant la quantité demandée de la quantité en stock
                    stock[concreteProduct]?.minus(quantity)
                    memento.save(this)
                    return Pair(concreteProduct, quantity)
                }
            }
        }
        return null
    }


    /**
     * Récupère l'instance d'un partenaire en fonction de son siret
     * @param siret numéro siret de l'entreprise
     * @return instance du partenaire si existant, null sinon
     */
    fun getPartner(siret: String): Shop? {
        Logger.console("Wholesaler::getPartner(...)")
        for (partner in partners) {
            if (partner.siret == siret) {
                return partner
            }
        }
        return null
    }


    /**
     * Traite soit une commande précise si un id est renseigné, soit toutes les commandes du grossiste sinon
     * @param orderId (optional) identifiant de la commande (unique)
     */
    fun process(orderId: Int = -1) {
        Logger.console("Wholesaler::process($orderId)")
        if (orderId >= 0) {
            var order: Order? = null
            var siret: String? = null
            val shop: Shop
            // On récupère la commande correspondant à l'id
            for ((_siret, orders) in shopOrders) {
                for (_order in orders) {
                    if (orderId == _order.id) {
                        siret = _siret
                        order = _order
                    }
                }
            }
            // On vérifie la solvabilité du client et on le débite
            // NOTA: Si aucune enseigne n'a été trouvée, on annule la préparation de la commande
            // On instancie le contenu de la livraison
            shop = getPartner(siret?:throw Exception("error: Wholesaler.process($orderId): no order linked with Shop#$siret"))
                    ?:return
            prepareAndDeliver(
                    arrayListOf(order?:throw Exception("error: Wholesaler.process($orderId): no order with this id")),
                    shop
            )
            memento.save(this)
        } else {
            throw Exception("✘ error ✘ Wholesaler.process($orderId): wrong input parameter, expected param ≥ 0")
        }
    }


    /**
     * Traite la liste de toutes les commandes d'une enseigne
     * @param partner entreprise dont il faut traiter les commandes
     */
    fun process(partner: Shop) {
        Logger.console("Wholesaler::process(Shop#${partner.siret})")
        val orders = shopOrders[partner.siret]
                ?:throw Exception("✘ error ✘ Wholesaler.process($partner): there is no partner with SIRET $partner.siret")
        prepareAndDeliver(orders, partner)
        memento.save(this)
    }


    /**
     * Traite toutes les commandes du grossiste
     */
    fun processAll() {
        Logger.console("Wholesaler::processAll()")
        for ((siret, orders) in shopOrders) {
            val shop = getPartner(siret)
                    ?:throw Exception("✘ error ✘ Wholesaler.processAll(): could not find shop with SIRET#$siret")
            prepareAndDeliver(orders, shop)
        }
        memento.save(this)
    }


    /**
     * Prepare l'ensemble des commandes pour transfert des produits concrets au client
     * @param orders    liste des commandes a traiter
     * @param shop      client detenteur de cette liste de commandes
     */
    private fun prepareAndDeliver(orders: MutableList<Order>, shop: Shop) {
        Logger.console("Wholesaler::prepareAndDeliver(...)")
        for (order in orders) {
            // S'il n'est pas solvable on ne traite pas la commande et on passe à la suivante
            if (!shop.bankAccount.debit(order.total)) {
                continue
            }
            val delivery: MutableList<Pair<ConcreteProduct, Int>> = ArrayList()
            for ((catalogProduct, quantity) in order.cart) {
                val basketEntry = withdraw(catalogProduct, quantity)?:break
                delivery.add(basketEntry)
            }
            order.processed = true
            deliver(shop, delivery)
        }
        memento.save(this)
    }


    /**
     * Livre le colis à l'entreprise correspondant au SIRET
     * @param shop entreprise réceptrice
     * @param parcels contenu de la commande
     */
    private fun deliver(shop: Shop, parcels: MutableList<Pair<ConcreteProduct, Int>>) {
        Logger.console("Wholesaler::deliver(...)")
        shop.receive(parcels)
        memento.save(this)
    }


    /**
     * Affichage en str des attributs du wholesaler
     */
    fun to_string(): String {
        var str = "⌘ siret: $siret" +
                "\n⌘ name: $name" +
                "\n⌘ n_employees: $nEmployees"
        return str
    }

    /**
     * Vérifie les dates de paiement différé, et procède au paiement s'il faut
     * @param date date actuelle fictive (gérée par l'AppContext)
     */
    fun deferredDebit(debitDate: LocalDate) {
        for (partner in partners) {
            if (partner.bankAccount.notify(debitDate)) {
                Logger.console("Deferred payment for Shop#${partner.siret}")
                AppContext.fetchAll()
            }
        }
    }
}