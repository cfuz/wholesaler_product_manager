package model.business.memento

import model.business.Wholesaler

class Memento {

    var backups: MutableList<WholesalerState> = ArrayList()



    fun lastState(): WholesalerState? {
        return backups.last()
    }

    fun previousState(): WholesalerState? {
        if (backups.isNotEmpty())
            return backups.removeAt(backups.size - 2)
        return null
    }

    fun state(index: Int): WholesalerState? {
        return backups[index]
    }

    fun save(wholesaler: Wholesaler) {
        val state = WholesalerState(wholesaler.partners, wholesaler.shopOrders, wholesaler.catalog, wholesaler.bankAccount)
        backups.add(state)
    }
}