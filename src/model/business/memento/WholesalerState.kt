package model.business.memento

import model.bank.BankAccount
import model.business.shop.Shop
import model.product.CatalogProduct
import model.product.Order

class WholesalerState(
        _partners: MutableList<Shop>,
        _shopOrders: HashMap<String, MutableList<Order>>,
        _catalog: MutableList<CatalogProduct>,
        _bankAccount: BankAccount
) {
    var partners: MutableList<Shop> = ArrayList(_partners)
    var shopOrders: HashMap<String, MutableList<Order>> = HashMap(_shopOrders)
    var catalog: MutableList<CatalogProduct> = ArrayList(_catalog)
    var bankAccount: BankAccount = _bankAccount
}