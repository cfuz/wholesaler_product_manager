package model.factory

import model.bank.BankAccount
import model.business.shop.FranchisedShop
import model.business.shop.Shop
import java.time.LocalDate


object FranchisedShopFactory: ShopFactory {
    override fun create(
            siret: String,
            hqSiret: String,
            name: String,
            nEmployee: Int,
            bankAccount: BankAccount,
            firstOrder: LocalDate,
            discount: Float,
            minimumQuantity: Float
    ): Shop {
        return FranchisedShop(
                siret,
                hqSiret,
                name,
                nEmployee,
                bankAccount,
                firstOrder,
                discount,
                minimumQuantity
        );
    }
}