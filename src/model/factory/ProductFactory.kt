package model.factory

import model.product.Product

/**
 * Cette classe sera un peu particulière dans le sens où elle jouera à la fois le rôle de Factory mais aussi de
 * FlyWeight afin d'éviter de réinstancier les mêmes produits
 */
object ProductFactory {
    var products: MutableList<Product> = ArrayList()


    fun get(name: String, category: String): Product {
        // On parcourt la liste des produit déjà créés par la Factory
        for (product in products) {
            if ((product.name == name) and (product.category == category)) {
                return product
            }
        }
        // Si nous ne trouvons rien nous devons alors instancier le produit afin de le stocker dans la liste de la
        // présente instance puis retourner cette dernière
        val product = Product(name, category)
        products.add(product)
        return product
    }
}