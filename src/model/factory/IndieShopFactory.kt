package model.factory

import model.bank.BankAccount
import model.business.shop.IndieShop
import model.business.shop.fidelity.LessThanTwoYears
import model.business.shop.Shop
import java.time.LocalDate

/**
 * Singleton Pattern
 * source: https://dev.to/silentsudo/create-a-simple-singleton-class-in-kotlin
 */
object IndieShopFactory: ShopFactory {
    override fun create(
            siret: String,
            hqSiret: String,
            name: String,
            n_employee: Int,
            bank_account: BankAccount,
            first_order: LocalDate,
            discount: Float,
            minimumQuantity: Float
    ): Shop {
        return IndieShop(
                siret,
                name,
                n_employee,
                bank_account,
                first_order,
                LessThanTwoYears()
        )
    }
}