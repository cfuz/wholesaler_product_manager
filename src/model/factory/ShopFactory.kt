package model.factory

import model.bank.BankAccount
import model.business.shop.Shop
import java.time.LocalDate


interface ShopFactory {
    fun create(
            siret: String,
            hqSiret: String = "None",
            name: String,
            nEmployee: Int,
            bankAccount: BankAccount,
            firstOrder: LocalDate,
            discount: Float = 0.0f,
            minimumQuantity: Float = 0.0f
    ): Shop
}