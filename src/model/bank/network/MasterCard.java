package model.bank.network;

public class MasterCard extends BankingNetwork {
	
	public MasterCard()
	{
		super(0.05f);
	}
	
	@Override
	public float getRefundingAmountWithFees(float amount)
	{
		return amount;
	}

	@Override
	public String toString() {
		return "Master Card";
	}
}
