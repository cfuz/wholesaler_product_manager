package model.bank.network;

import model.bank.network.rate.CountryRate;

public class Visa extends BankingNetwork {

	private CountryRate countryRate;



	public Visa(CountryRate cr) {
		super(0.025f);
		this.countryRate = cr;
	}


	@Override
	public float getRefundingAmountWithFees(float amount)
	{
		return countryRate.refundCalculation(amount);
	}


	public CountryRate getCountryRate()
	{
		return countryRate;
	}


	@Override
	public String toString() {
		return "Visa";
	}


	@Override
	public String country() {
		return countryRate.country();
	}
}
