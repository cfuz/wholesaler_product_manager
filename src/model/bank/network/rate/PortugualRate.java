package model.bank.network.rate;

public class PortugualRate implements CountryRate {
	@Override
	public float refundCalculation(float amount) {
		return amount - amount*0.015f;
	}

	@Override
	public String country() {
		return "Portugal";
	}
}