package model.bank.network.rate;

public class OthersRate implements CountryRate {
	@Override
	public float refundCalculation(float amount) {
		return amount - amount*0.030f;
	}

	@Override
	public String country() {
		return "Other";
	}
}
