package model.bank.network.rate;

public class SpainRate implements CountryRate {
	@Override
	public float refundCalculation(float amount) {
		return amount - amount*0.010f;
	}

	@Override
	public String country() {
		return "Spain";
	}
}