package model.bank.network.rate;

public class USARate implements CountryRate {
	@Override
	public float refundCalculation(float amount) {
		return amount - amount*0.015f;
	}

	@Override
	public String country() {
		return "USA";
	}
}
