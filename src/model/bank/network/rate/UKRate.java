package model.bank.network.rate;

public class UKRate implements CountryRate {
	@Override
	public float refundCalculation(float amount) {
		return amount - amount*0.025f;
	}

	@Override
	public String country() {
		return "United Kingdoms";
	}
}
