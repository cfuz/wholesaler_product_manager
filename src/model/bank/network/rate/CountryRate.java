package model.bank.network.rate;

public interface CountryRate {
	float refundCalculation(float amount);
	String country();
}
