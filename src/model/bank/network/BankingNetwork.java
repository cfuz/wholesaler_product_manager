package model.bank.network;

public abstract class BankingNetwork {
	private float bankFees;


	public BankingNetwork(float bankFees) {
		this.bankFees = bankFees;
	}
	
	public float getDebitAmountWithFees(float amount)
	{
		return amount + bankFees*amount;
	}
	
	public abstract float getRefundingAmountWithFees(float amount);

	public abstract String toString();

	public String country() {
		return null;
	}
}
