package model.bank;

import model.bank.memento.BankingOperation;
import model.bank.network.BankingNetwork;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public abstract class BankAccount {
	protected float balance;
	protected BankingNetwork bankingNetwork;
	protected List<BankingOperation> bankingOperations;


	public BankAccount(float balance, BankingNetwork bankingNetwork) {
		this.bankingNetwork = bankingNetwork;
		this.balance = balance;
		this.bankingOperations = new ArrayList<>();
	}

	public float getBalance() {
		return balance;
	}

	public BankingNetwork getBankingNetwork() {
		return bankingNetwork;
	}

	public void setBalance(float balance) {
		this.balance = balance;
		saveState();
	}

	public void setBankingNetwork(BankingNetwork bankingNetwork) {
		this.bankingNetwork = bankingNetwork;
		saveState();
	}

	public boolean notify(LocalDate actualDate) { return false; }

	public abstract boolean debit(float amount);
	public abstract boolean credit(float amount);
	public  abstract void saveState();
	public abstract void restoreLastState();
	public abstract void restoreStateByIndex(int index);
}
