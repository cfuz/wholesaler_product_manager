package model.bank;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import app.AppContext;
import model.bank.memento.ClientBankingOperation;
import model.bank.network.BankingNetwork;


public class ClientBankAccount extends BankAccount {
	private int deferredPaymentDay;
	private List<Float> debitOperations;
	
	public ClientBankAccount(float balance, BankingNetwork bankingNetwork, int day)
	{
		super(balance, bankingNetwork);
		this.deferredPaymentDay = day;
		debitOperations = new ArrayList<>();
	}

	@Override
	public boolean debit(float amount)
	{
		float checkBalance = this.balance;
		for(float operation : this.debitOperations)
		{
			checkBalance -= operation;
		}
		float checkAmount = bankingNetwork.getDebitAmountWithFees(amount);
		if(checkBalance - checkAmount >= 0)
		{
			debitOperations.add(amount);
			saveState();
			return true;
		}
		
		return false;
	}

	@Override
	public boolean credit(float amount)
	{
		float realRefundingAmount = this.bankingNetwork.getRefundingAmountWithFees(amount);
		this.balance += realRefundingAmount;
		saveState();
		return true;
	}

	public void setDeferredPaymentDay(int day)
	{
		this.deferredPaymentDay = day;
	}

	private boolean doDebitAtDifferedPaymentDay(LocalDate actualDate)
	{
		if(actualDate.getDayOfMonth() == deferredPaymentDay)
		{
			float debitAmount = 0.0f;
			for(double operation : debitOperations)
			{
				debitAmount += operation;
			}
			debitOperations.clear();
			if(debitAmount > 0)
			{
				this.balance -= bankingNetwork.getDebitAmountWithFees(debitAmount);
				saveState();
				AppContext.getWholesaler().getBankAccount().credit(debitAmount);
				AppContext.fetchAll();
				return true;
			}
			return false;
		}
		return false;
	}

	@Override
	public boolean notify(LocalDate actualDate)
	{
		return doDebitAtDifferedPaymentDay(actualDate);
	}

	@Override
	public void saveState() {
		this.bankingOperations.add(new ClientBankingOperation(this.balance, this.bankingNetwork, this.deferredPaymentDay, this.debitOperations));
	}

	@Override
	public void restoreLastState() {
		if(!this.bankingOperations.isEmpty())
		{
			ClientBankingOperation previousState = (ClientBankingOperation) this.bankingOperations.get(this.bankingOperations.size() - 1);
			this.balance = previousState.getBalance();
			this.bankingNetwork = previousState.getBankingNetwork();
			this.deferredPaymentDay = previousState.getDeferredPaymentDay();
			this.debitOperations = previousState.getDebitOperations();
			saveState(); // on sauvegarde le nouveau état
		}
	}

	@Override
	public void restoreStateByIndex(int index) {
		if(!this.bankingOperations.isEmpty())
		{
			ClientBankingOperation previousState = (ClientBankingOperation) this.bankingOperations.get(index);
			this.balance = previousState.getBalance();
			this.bankingNetwork = previousState.getBankingNetwork();
			this.deferredPaymentDay = previousState.getDeferredPaymentDay();
			this.debitOperations = previousState.getDebitOperations();
			saveState(); // on sauvegarde le nouveau état
		}
	}
}
