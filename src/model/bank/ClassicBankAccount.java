package model.bank;

import model.bank.memento.ClassicBankingOperation;
import model.bank.network.BankingNetwork;

public class ClassicBankAccount extends BankAccount {
	public ClassicBankAccount(float balance, BankingNetwork bankingNetwork) {
		super(balance, bankingNetwork);
	}

	@Override
	public boolean debit(float amount) {
		float realDebitAmount = this.bankingNetwork.getDebitAmountWithFees(amount);
		if(this.balance - realDebitAmount >= 0) {
			balance -= realDebitAmount;
			saveState();
			return true;
		}
		return false;
	}

	@Override
	public boolean credit(float amount) {
		float realRefundingAmount = this.bankingNetwork.getRefundingAmountWithFees(amount);
		this.balance += realRefundingAmount;
		saveState();
		return true;
	}

	@Override
	public void saveState() {
		this.bankingOperations.add(new ClassicBankingOperation(this.balance, this.bankingNetwork));
	}

	@Override
	public void restoreLastState() {
		if(!this.bankingOperations.isEmpty()) {
			ClassicBankingOperation previousState = (ClassicBankingOperation) this.bankingOperations.get(this.bankingOperations.size() - 1);
			this.balance = previousState.getBalance();
			this.bankingNetwork = previousState.getBankingNetwork();
			saveState(); // on sauvegarde le nouveau état
		}
	}

	@Override
	public void restoreStateByIndex(int index) {
		if(!this.bankingOperations.isEmpty()) {
			ClassicBankingOperation previousState = (ClassicBankingOperation) this.bankingOperations.get(index);
			this.balance = previousState.getBalance();
			this.bankingNetwork = previousState.getBankingNetwork();
			saveState(); // on sauvegarde le nouveau état
		}
	}
}
