package model.bank.memento;

import model.bank.network.BankingNetwork;

public class ClassicBankingOperation extends BankingOperation {
    public ClassicBankingOperation(float balance, BankingNetwork bankingNetwork) {
        super(balance, bankingNetwork);
    }
}
