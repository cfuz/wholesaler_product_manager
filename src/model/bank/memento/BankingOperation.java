package model.bank.memento;

import model.bank.network.BankingNetwork;


public abstract class BankingOperation {
    protected float balance;
    protected BankingNetwork bankingNetwork;


    public BankingOperation(float balance, BankingNetwork bankingNetwork) {
        this.balance = balance;
        this.bankingNetwork = bankingNetwork;
    }

    public float getBalance() {
        return balance;
    }

    public BankingNetwork getBankingNetwork() {
        return bankingNetwork;
    }
}
