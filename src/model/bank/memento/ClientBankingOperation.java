package model.bank.memento;

import model.bank.network.BankingNetwork;

import java.time.LocalDate;
import java.util.List;


public class ClientBankingOperation extends BankingOperation {
    private int deferredPaymentDay;
    private List<Float> debitOperations;


    public ClientBankingOperation(
            float balance,
            BankingNetwork bankingNetwork,
            int deferredPaymentDay,
            List<Float> debitOperations
    ) {
        super(balance, bankingNetwork);
        this.deferredPaymentDay = deferredPaymentDay;
        this.debitOperations = debitOperations;
    }

    public int getDeferredPaymentDay() {
        return this.deferredPaymentDay;
    }

    public List<Float> getDebitOperations() {
        return this.debitOperations;
    }
}
